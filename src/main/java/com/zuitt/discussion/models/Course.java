package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table(name="courses")
public class Course {

    @Column
    private boolean isActive;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column
    private String name;
    @Column
    private String description;

    @Column
    private double price;
    @OneToMany(mappedBy ="course",cascade= CascadeType.ALL)
    @JsonIgnore
    Set<CourseEnrollment> enrollees;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }



//    @OneToMany(mappedBy = "course")
//    Set<Enrollment> enrolees;





    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }



    public Set<CourseEnrollment> getEnrollees() {
        return enrollees;
    }

    public void setEnrollees(Set<CourseEnrollment> enrollees) {
        this.enrollees = enrollees;
    }
    public void addEnrollee(CourseEnrollment courseEnrollment) {
        this.enrollees.add(courseEnrollment );
        courseEnrollment.setCourse(this);
    }




}
