package com.zuitt.discussion.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="enrollments")

public class CourseEnrollment {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JoinColumn(name="user_id",referencedColumnName = "id")

    private User user;


    @ManyToOne
    @JoinColumn(name="course_id",referencedColumnName = "id")
    private Course course;


    @Column
    private LocalDateTime dateTime;
    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }
    public LocalDateTime getDateTime() {
        return dateTime;
    }
//    @Column
//    private String title;
//
//    @Column
//    private String content;


    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }


    public CourseEnrollment(){}
    public CourseEnrollment(String title,String content){
//        this.title=title;
//        this.content=content;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//    }
//
//    public String getContent() {
//        return content;
//    }
//
//    public void setContent(String content) {
//        this.content = content;
//    }

    public User getUser(){
        return user;
    }
    public void setUser(User user){
        this.user=user;
    }
}

