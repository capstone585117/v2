package com.zuitt.discussion.controllers;

import com.zuitt.discussion.config.JwtToken;
import com.zuitt.discussion.models.Course;
import com.zuitt.discussion.models.CourseEnrollment;
import com.zuitt.discussion.repositories.CourseEnrollmentRepository;
import com.zuitt.discussion.repositories.CourseRepository;
import com.zuitt.discussion.repositories.UserProjection;
import com.zuitt.discussion.services.CourseEnrollmentService;
import com.zuitt.discussion.services.CourseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class CourseController {
    @Autowired
    CourseService courseService;
    @Autowired
    CourseEnrollmentService courseEnrollmentService;
    @Autowired
    CourseEnrollmentRepository courseEnrollmentRepository;
    @Autowired
    CourseRepository courseRepository;
    @Autowired
    JwtToken jwtToken;
    @PostMapping("/courses")
    public ResponseEntity createCourse(@RequestBody Course course){
        return courseService.createCourse(course);
    }
    @GetMapping("/courses")
    public Iterable<Course> getAllCourses(){
        return courseService.getAllCourses();
    }
    @GetMapping("/activeCourses")
    public Iterable<Course> getAllActiveCourses(){
        return courseService.getAllActiveCourses();
    }
    @GetMapping("/courses/{courseid}")
    public ResponseEntity getCourse(@PathVariable(name = "courseid")Long id){
        return courseService.getCourse(id);
    }
    @PutMapping("/courses/{courseid}")
    public ResponseEntity createCourse(@RequestBody Course course,@PathVariable(name = "courseid")Long id){
        return courseService.updateCourse(course,id);
    }
    @DeleteMapping("/courses/{courseid}")
    public ResponseEntity deleteCourse(@PathVariable(name = "courseid")Long id){
        return courseService.deleteCourse(id);
    }
    @PutMapping("/recover/{courseid}")
    public ResponseEntity recoverCourse(@PathVariable(name = "courseid")Long id){
        return courseService.Unarchive(id);
    }
    @PostMapping("/enroll/{courseid}")
    public ResponseEntity enrollCourse(@RequestHeader(name="Authorization") String stringToken, @PathVariable(name = "courseid")Long id){
        return courseEnrollmentService.enrollStudent(stringToken,id);
    }
    @GetMapping("/enrollments")
    public Iterable<CourseEnrollment> enrollCourse(){
        return  courseEnrollmentRepository.findAll();
    }
    @GetMapping("/courses/{courseid}/enrollees")
    public Iterable<?> getCourseEnrollments(@PathVariable(name = "courseid")Long id){

        return courseService.getAllEnrollees(id);
    }


}
